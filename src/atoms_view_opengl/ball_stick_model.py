from typing import Tuple, List, Generator

import numpy as np

from ase import Atoms
from ase.cell import Cell
from ase.data import chemical_symbols
from ase.constraints import constrained_indices
from atoms_view_opengl.ball_kind import BallKind
from atoms_view_opengl.utils.find_bonds import find_bonds


class BallStickModel:
    """ The class describes the positions of atoms and the bonds between them """

    def __init__(self, atoms: Atoms):
        """
        Args:
            atoms: the ASE.Atoms object.
        """
        self.atomic_numbers = atoms.get_atomic_numbers()
        self.positions = atoms.get_positions()
        self.positions -= atoms.get_center_of_mass()
        self.atomic_tags = atoms.get_tags()
        self.cell: Cell = atoms.get_cell()
        self.is_cell_visible: bool = np.linalg.norm(self.cell) > 0.0
        self.atomic_labels = []

        self.atom2kind, self.kind2desc = self.init_ball_kinds_unique(atoms)

        self.bonds: np.ndarray = find_bonds(self.positions, self.atomic_numbers)
        self.bond_radius: float = 0.1

        self.atom2color: np.ndarray = self.kind2color[self.atom2kind]
        self.atom2radius: np.ndarray = self.kind2radius[self.atom2kind]

        atom2scr_rot = self.kind2needs_scr_rot[self.atom2kind]
        self.atom_scr_rot2pos = self.positions[atom2scr_rot]
        self.atom_scr_rot2kind = self.atom2kind[atom2scr_rot]
        self.atom_non_rot2pos = self.positions[~atom2scr_rot]
        self.atom_non_rot2kind = self.atom2kind[~atom2scr_rot]

        ci = constrained_indices(atoms).astype(dtype=int)
        self.atom2fixed = np.zeros(len(self.atomic_numbers))
        self.atom2fixed[ci] = 1
        self.is_constrained = len(ci) > 0
        self.atom_fixed2pos = self.positions[ci]
        self.atom_fixed2kind = self.atom2kind[ci]

    def __repr__(self):
        return f'BallStickModel: {vars(self)}'

    def set_atomic_labels_type(self, label_type: int):
        self.atomic_labels = [' '] * len(self.atomic_numbers)
        for index in range(len(self.atomic_labels)):
            if label_type == 1:
                self.atomic_labels[index] = str(chemical_symbols[self.atomic_numbers[index]])
            elif label_type == 2:
                self.atomic_labels[index] = str(index)
            elif label_type == 3:
                self.atomic_labels[index] = "c" if self.atom2fixed[index] else "f"
            elif label_type == 4:
                self.atomic_labels[index] = str(self.atomic_tags[index])

    def change_atomic_colors(self):
        for kind in self.kind2desc:
            kind.change_atomic_color()
        self.atom2color = self.kind2color[self.atom2kind]

    def get_radius(self, index: int) -> float:
        """
        Returns the atomic radius.
        Args:
            index: atomic index.
        Returns:
            - float --> radius of index atom
        """
        return self.atom2radius[index]

    @staticmethod
    def init_ball_kinds_unique(atoms: Atoms) -> Tuple[np.ndarray, List[BallKind], bool]:
        """
        Identify the atomic balls according to their chemical species and constrainedness.
        Args:
            atoms: ASE Atoms object.
        Returns:
            - Array atom_index --> ball_kind_index,
            - List of corresponding BallKinds objects, i.e. ball_kind_index --> ball description,
            - A boolean indicating if there was at least one constrained atom.
        """
        qa2v = np.zeros((2, len(atoms)), dtype=int)  # (quantity, atom) --> value
        qa2v[0, :] = atoms.get_atomic_numbers()
        qa2v[1, :] = atoms.get_tags()
        unique_balls, atom2kind = np.unique(qa2v, return_inverse=True, axis=1)
        kind2desc = [BallKind(n, int(t)) for n, t in unique_balls.T]
        return atom2kind, kind2desc

    @property
    def kind2color(self) -> np.ndarray:
        kind2color = np.zeros((self.num_atom_kinds, 4), dtype='float32')
        for kind_index, desc in enumerate(self.kind2desc):
            kind2color[kind_index] = desc.color
        return kind2color

    @property
    def kind2radius(self) -> np.ndarray:
        kind2radius = np.zeros(self.num_atom_kinds, dtype='float32')
        for kind_index, desc in enumerate(self.kind2desc):
            kind2radius[kind_index] = desc.radius
        return kind2radius

    @property
    def kind2needs_scr_rot(self) -> np.ndarray:
        kind2scr_rot = np.zeros(self.num_atom_kinds, dtype=bool)
        for kind_index, desc in enumerate(self.kind2desc):
            kind2scr_rot[kind_index] = desc.needs_screen_rotation
        return kind2scr_rot

    @property
    def num_atoms(self) -> int:
        """
        Number of atoms.
        Returns:
            Number of atoms
        """
        return len(self.positions)

    @property
    def num_atom_kinds(self) -> int:
        """
        Number of kinds of atoms.
        Returns:
            The number
        """
        return len(self.kind2desc)

    @property
    def bonds_colors(self) -> Tuple[np.ndarray, np.ndarray]:
        """
        Constructs a tuple of two arrays:
         - bond index --> color of the first end,
         - bond index --> color of the second end
        Returns:
          The tuple of arrays.
        """
        return self.atom2color[self.bonds[0]], self.atom2color[self.bonds[1]]

    @property
    def bonds_positions(self) -> Tuple[np.ndarray, np.ndarray, np.ndarray]:
        """
        Constructs a tuple of three arrays:
         - bond index --> coordinate of first end,
         - bond index --> coordinate of the midpoint,
         - bond index --> coordinate of the second end.
        Returns:
          The tuple of arrays.
        """
        pos1 = self.positions[self.bonds[0]]
        pos2 = self.positions[self.bonds[1]]

        rel = pos2 - pos1
        rel_norm = rel / np.linalg.norm(rel, axis=1)[:, None]

        radii = np.sqrt(self.atom2radius ** 2 - self.bond_radius ** 2)

        hh1 = radii[self.bonds[0]]
        pos1 += hh1[:, None] * rel_norm

        hh2 = radii[self.bonds[1]]
        pos2 -= hh2[:, None] * rel_norm
        return pos1, 0.5 * (pos1 + pos2), pos2
