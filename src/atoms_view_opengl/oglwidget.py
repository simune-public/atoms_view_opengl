from typing import Optional, Tuple

from qtpy.QtWidgets import QWidget, QOpenGLWidget
from qtpy.QtCore import Qt
from qtpy.QtGui import QKeyEvent

from ase import Atoms

from atoms_view_opengl.openglcore import OpenGLCore
from atoms_view_opengl.ball_stick_model import BallStickModel


NO_ROTATION = '0x,0y,0z'


class OGLWidget(QOpenGLWidget):
    CLEAR_COLOR = (.9, .9, .9, 1.0)

    """ This class implements a widget for displaying atomic systems using an OpenGL """
    def __init__(self, parent: Optional[QWidget] = None, rotation: str = NO_ROTATION, *,
                 clear_color: Tuple[float, float, float, float] = CLEAR_COLOR):
        super().__init__(parent)
        self.setFocusPolicy(Qt.StrongFocus)
        self.atoms: Optional[Atoms] = None
        self.gl_core: OpenGLCore = OpenGLCore(make_current_callback=self.makeCurrent,
                                              width_callback=self.width,
                                              height_callback=self.height,
                                              clear_color=clear_color)
        self.rotation: str = NO_ROTATION if rotation == '' else rotation

    def wheelEvent(self, event):
        if self.gl_core.scale(event.angleDelta().y()):
            self.update()

    def mouseMoveEvent(self, event):
        """ The right mouse button is used to rotate the model """
        if event.buttons() == Qt.RightButton:
            if self.gl_core.rotate(event.x(), event.y(), self.width(), self.height()):
                self.update()

        if event.buttons() == Qt.LeftButton:
            if self.gl_core.pan(event.x(), event.y(), self.width(), self.height()):
                self.update()

    def mousePressEvent(self, event):
        self.gl_core.set_cursor_coord(event.x(), event.y())

    def keyReleaseEvent(self, event: QKeyEvent):
        if event.key() == Qt.Key_B:
            self.gl_core.switch_clear_color()
            self.update()
        elif event.key() == Qt.Key_L:
            self.gl_core.switch_label_type()
            self.update()
        elif event.key() == Qt.Key_C:
            self.gl_core.switch_atoms_color_type()
            self.update()
        else:
            super().keyReleaseEvent(event)

    def clear(self):
        self.gl_core.clear()

    def initializeGL(self):
        """ This method is called from inside the .show() method. """
        super().initializeGL()
        self.gl_core.initialize_gl()
        if self.atoms is not None:
            self.set_structure(self.atoms)

    def paintGL(self):
        """ This method will be called when doing rotations, pan and zoom operations. """
        super().paintGL()
        self.gl_core.paint_gl()

    def plot(self, atoms: Atoms) -> None:
        """
        This method could be called even before self.show()
        Args:
            atoms: the ASE.Atoms object.
        """
        self.atoms = atoms.copy()
        if self.isVisible():
            self.set_structure(atoms)

    def set_structure(self, atoms: Atoms):
        """
        Converting an ASE atoms object into an internal data format for building 3D models
        Args:
            atoms: the ASE.Atoms object.

        Returns:
            None
        """
        ball_stick_model = BallStickModel(atoms)
        self.set_opengl_data(ball_stick_model)

        angles = [float(angle_axis[:-1]) for angle_axis in self.rotation.split(',')]
        self.gl_core.set_rotations_angle(angles)

    def set_opengl_data(self, ball_stick_model: BallStickModel):
        """
        The function creates QuadObjS objects for atoms and bonds
        Args:
            ball_stick_model: BallAndStickModel - all information about model

        Returns:
            None
        """
        self.gl_core.set_opengl_data(ball_stick_model)
