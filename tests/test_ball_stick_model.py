import pytest
import numpy as np

from ase.constraints import FixAtoms


def test_ball_and_stick_model(ball_stick_model):
    pos = ball_stick_model.bonds_positions
    assert isinstance(pos, tuple)

    colors = ball_stick_model.bonds_colors
    assert isinstance(colors, tuple)
    assert ball_stick_model.num_atoms == 3
    assert ball_stick_model.num_atom_kinds == 2
    assert 'BallStickModel' in str(ball_stick_model)
    assert ball_stick_model.bond_radius == pytest.approx(0.1)
    i_kind = ball_stick_model.atom2kind[0]
    assert ball_stick_model.get_radius(0) == pytest.approx(ball_stick_model.kind2radius[i_kind])


def test_ball_and_stick_model_constrained_atoms(get_ball_and_stick_model, water_atoms):
    water_atoms.set_constraint(FixAtoms(indices=[0, 2]))
    model = get_ball_and_stick_model(water_atoms)
    assert model.num_atom_kinds == 2


def test_ball_and_stick_model_atoms_label(get_ball_and_stick_model, water_atoms):
    water_atoms.set_constraint(FixAtoms(indices=[0, 2]))
    model = get_ball_and_stick_model(water_atoms)
    model.set_atomic_labels_type(1)
    assert model.atomic_labels[0] == 'O'
    model.set_atomic_labels_type(2)
    assert model.atomic_labels[0] == '0'
    model.set_atomic_labels_type(3)
    assert model.atomic_labels[0] == 'c'
    model.set_atomic_labels_type(4)
    assert model.atomic_labels[0] == '0'


def test_ball_stick_model_bond_positions(ball_stick_model):
    pos1, center, pos2 = ball_stick_model.bonds_positions
    y, z = 0.24781635, -0.12688479
    pos1_ref = np.array(((0., y, z), (0., -y, z)))
    y, z = 0.45886676, 0.29177581
    center_ref = np.array(((0., y, -z), (0., -y, -z)))
    y, z = 0.66991718, -0.45666684
    pos2_ref = np.array(((0., y, z),
                         (0., -y, z)))
    assert pos1 == pytest.approx(pos1_ref), f'\n pos1 = {pos1},\n pos1_ref = {pos1_ref}\n'
    assert center == pytest.approx(center_ref)
    assert pos2 == pytest.approx(pos2_ref), f'\n pos2 = {pos2},\n pos2_ref = {pos2_ref}\n'


def test_ball_stick_model_needs_scr_rotation(get_ball_and_stick_model, water_atoms_h1_ghost):
    model = get_ball_and_stick_model(water_atoms_h1_ghost)
    assert model.num_atom_kinds == 3
    assert len(model.atom_scr_rot2kind) == len(model.atom_scr_rot2pos) == 1
    assert model.atom_scr_rot2pos[0] == pytest.approx(model.positions[1])

    kind = model.atom_scr_rot2kind[0]
    assert model.kind2desc[kind].needs_screen_rotation

    assert len(model.atom_non_rot2kind) == len(model.atom_non_rot2pos) == 2
    assert model.atom_non_rot2pos[0] == pytest.approx(model.positions[0])
    assert model.atom_non_rot2pos[1] == pytest.approx(model.positions[2])
    for kind in model.atom_non_rot2kind:
        assert not model.kind2desc[kind].needs_screen_rotation
