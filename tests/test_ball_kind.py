from atoms_view_opengl.ball_kind import BallKind


def test_ball_kind():
    ball_desc = BallKind(1, 0)
    assert 'BallKind' in str(ball_desc)
