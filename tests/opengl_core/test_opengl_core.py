import numpy as np
import pytest


def test_opengl_core_is_active(ogl_core_object, ball_stick_model):
    assert ogl_core_object.is_active
    ogl_core_object.clear()
    assert not ogl_core_object.is_active
    ogl_core_object.set_opengl_data(ball_stick_model)
    assert ogl_core_object.rotate(100, 100, 640, 480)
    assert ogl_core_object.rotation_angles == pytest.approx((-37.5, -28.125, 0.0))

    ogl_core_object.clear()
    ogl_core_object.set_opengl_data(ball_stick_model)

    assert pytest.approx(ogl_core_object.num_tex) == 1


def test_ogl_core_set_xy_scale(ogl_core_object):
    ogl = ogl_core_object
    assert ogl.set_cursor_coord(100, 200)
    assert not ogl.set_cursor_coord(100, 200)
    assert pytest.approx(100) == ogl.xs_old
    assert pytest.approx(200) == ogl.ys_old

    cam_pos_old = ogl.cam_position[2]
    ogl.scale(120.0)
    assert ogl.cam_position[2] - cam_pos_old == pytest.approx(0.1)
    ogl.scale(-120.0)
    assert ogl.cam_position[2] - cam_pos_old == pytest.approx(0.0)


def test_ogl_core_rotation(ogl_core_object):
    angles = np.array((10.0, 20.0, 30.0))
    ogl_core_object.set_rotations_angle(angles)
    assert pytest.approx(angles) == ogl_core_object.rotation_angles


def test_exceptions_in_paint_gl(ogl_core_object, mocker, capsys):

    error_message = 'Some Error 231'

    def mock_light_prepare():
        raise RuntimeError(error_message)

    mocker.patch.object(ogl_core_object, 'light_prepare', mock_light_prepare)
    ogl_core_object.paint_gl()
    captured = capsys.readouterr()
    assert captured.out.startswith(error_message)


def test_ogl_core_draw_cell(ogl_core_object, get_ball_and_stick_model, silver_atoms):
    ball_and_stick_model = get_ball_and_stick_model(silver_atoms)
    ogl_core_object.set_opengl_data(ball_and_stick_model)
    ogl_core_object.paint_gl()

