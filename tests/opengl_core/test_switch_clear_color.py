
def test_ogl_core_switch_clear_color(ogl_core_object):
    assert ogl_core_object.clear_color_index == -1
    for index_ref in range(len(ogl_core_object.clear_colors)):
        ogl_core_object.switch_clear_color()
        assert ogl_core_object.clear_color_index == index_ref
    ogl_core_object.switch_clear_color()
    assert ogl_core_object.clear_color_index == 0
