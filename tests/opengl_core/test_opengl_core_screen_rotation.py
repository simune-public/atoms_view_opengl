import pytest


class MyData:
    def __init__(self):
        self.kind = None
        self.pos = None


MY_SECRET_KIND = 5463


def test_ogl_core_screen_rotation_paint_gl(get_ogl_core, water_atoms_h1_ghost, mocker):
    core = get_ogl_core(water_atoms_h1_ghost)

    model = core.ball_stick_model
    # This test can handle only one ghost atom
    assert len(model.atom_scr_rot2kind) == 1

    data = MyData()

    def mocked_add_atom_screen_rotation(kind, pos):
        data.kind, data.pos = kind, pos

    mocker.patch.object(core, 'add_atom_screen_rotation', mocked_add_atom_screen_rotation)
    core.paint_gl()
    assert model.kind2desc[data.kind].needs_screen_rotation
    assert model.atom_scr_rot2pos[0] == pytest.approx(data.pos)


def test_ogl_core_screen_rotation_paint_gl(get_ogl_core, water_atoms_h1_ghost, mocker):
    core = get_ogl_core(water_atoms_h1_ghost)

    data = MyData()

    def mock_apply_screen_rotation():
        data.kind = MY_SECRET_KIND

    mocker.patch.object(core, 'apply_screen_rotation', mock_apply_screen_rotation)
    core.paint_gl()
    assert data.kind == MY_SECRET_KIND

