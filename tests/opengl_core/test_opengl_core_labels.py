
def test_ogl_core_switch_label_type(ogl_core_object):
    assert ogl_core_object.label_type == 0
    for index_ref in range(1, 5):
        ogl_core_object.switch_label_type()
        assert ogl_core_object.label_type == index_ref

    ogl_core_object.switch_label_type()
    assert ogl_core_object.label_type == 0


def test_ogl_core_render_labels(get_ogl_core, water_atoms_h1_constrained):
    ogl_core = get_ogl_core(water_atoms_h1_constrained)
    assert ogl_core.label_type == 0
    ogl_core.switch_label_type()
    ogl_core.paint_gl()
