from typing import Callable
import pytest

from ase import Atoms

from atoms_view_opengl.openglcore import OpenGLCore


@pytest.fixture
def get_ogl_core(get_ogl_widget) -> Callable[[Atoms], OpenGLCore]:

    def factory_function(atoms) -> OpenGLCore:
        widget = get_ogl_widget(atoms)
        return widget.gl_core

    return factory_function


@pytest.fixture
def ogl_core_object(get_ogl_core, water_atoms) -> OpenGLCore:
    return get_ogl_core(water_atoms)
