from ase import Atoms
from ase.constraints import FixAtoms


def test_is_active(ogl_widget_water_atoms, water_atoms):
    widget = ogl_widget_water_atoms
    assert widget.gl_core.is_active
    widget.clear()
    assert not widget.gl_core.is_active
    widget.set_structure(water_atoms)
    widget.plot(water_atoms)
    assert widget.gl_core.is_active


def test_is_work_with_constrained_atoms(ogl_widget_water_atoms, water_atoms):
    widget = ogl_widget_water_atoms
    assert not widget.gl_core.ball_stick_model.is_constrained
    widget.clear()
    water_atoms.set_constraint(FixAtoms(indices=[1]))
    widget.plot(water_atoms)
    assert widget.gl_core.ball_stick_model.is_constrained
    assert len(widget.gl_core.ball_stick_model.atom2kind) == 3
    assert len(widget.gl_core.ball_stick_model.kind2desc) == 2


def test_plot_before_show(ogl_widget_plot_before_show):
    widget = ogl_widget_plot_before_show
    assert isinstance(widget.atoms, Atoms)
    widget.show()
    # xxx not sure how to assert the model is shown


def test_widget_atoms_label(ogl_widget_water_atoms, water_atoms):
    widget = ogl_widget_water_atoms
    widget.clear()
    widget.set_structure(water_atoms)
    widget.gl_core.label_type = 3
    assert widget.gl_core.is_labels_visible
    widget.show()
    widget.clear()
    widget.set_structure(water_atoms)
    assert not widget.gl_core.is_labels_visible
