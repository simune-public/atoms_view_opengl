from timeit import default_timer
import pytest
import numpy as np

import ase.io

from atoms_view_opengl.utils.find_bonds import find_bonds, find_bonds_brute_force


def test_find_bonds_benchmark(tests_path, water_atoms):
    ref_data_path = tests_path / 'ref_data'

    # file_path = ref_data_path / '2JWQ_solv.xyz'
    # file_path = ref_data_path / 'dna0002.xyz'
    file_path = ref_data_path / 'DNA-1.xyz'
    atoms = ase.io.read(file_path)

    # atoms = water_atoms
    print()
    print(atoms.get_chemical_formula(), len(atoms))

    pos, zz = atoms.get_positions(), atoms.get_atomic_numbers()

    time1 = default_timer()
    bonds = find_bonds(pos, zz)
    time2 = default_timer()
    print('find_bonds (2-loop)', len(bonds[0]), time2 - time1)

    if len(pos) < 1000:
        time3 = default_timer()
        bonds_brute_force = find_bonds_brute_force(pos, zz)
        time4 = default_timer()
        print('find_bonds_brute_force', len(bonds_brute_force[0]), time4 - time3)
        assert len(bonds_brute_force[0]) == len(bonds[0])


def test_utils_find_bonds(water_atoms):
    positions = water_atoms.get_positions()
    atomic_numbers = water_atoms.get_atomic_numbers()
    bonds = find_bonds(positions, atomic_numbers)
    assert np.array(bonds) == pytest.approx(np.array(((0, 0), (1, 2))))


def test_utils_find_bonds_methods(water_atoms):
    positions = water_atoms.get_positions()
    atomic_numbers = water_atoms.get_atomic_numbers()
    bonds = find_bonds(positions, atomic_numbers)
    bonds3 = find_bonds_brute_force(positions, atomic_numbers)
    assert np.array(bonds) == pytest.approx(np.array(((0, 0), (1, 2))))
    assert np.array(bonds) == pytest.approx(np.array(bonds3))
