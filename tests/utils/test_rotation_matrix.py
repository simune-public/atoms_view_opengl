import numpy as np
import pytest

from atoms_view_opengl.utils.rotation_matrix import get_rotation_matrix_sph

MAT_REF_1 = np.eye(4)
MAT_REF_1[2, 2] = -1.0

MAT_REF_2 = np.array(
    ((-0.78072006, -0.39036003, -0.48795003, 0.),
     (-0.4472136, 0.8944272, 0., 0.),
     (0.43643576, 0.21821788, -0.8728716, 0.),
     (0., 0., 0., 1.)))


@pytest.mark.parametrize('vec, mat_ref', (((0.0, 0.0, 1.0), np.eye(4)),
                                          ((0.0, 0.0, -2.0), MAT_REF_1),
                                          ((1.0, 0.5, -2.0), MAT_REF_2)))
def test_ogl_core_rotation_matrix(vec, mat_ref):

    rot_mat = get_rotation_matrix_sph(vec)
    assert isinstance(rot_mat, np.ndarray)
    assert rot_mat == pytest.approx(mat_ref)
