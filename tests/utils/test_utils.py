import numpy as np
import pytest
from atoms_view_opengl.utils.read_image import read_image
from atoms_view_opengl.utils.rotation_matrix import compute_rotation_matrix_xyz


def test_utils_read_image():
    data = read_image('verdana_alpha.png')
    assert data.shape == (512, 512, 4)
    assert data.dtype == 'uint8'


def test_utils_matrix_of_rotation():
    assert pytest.approx(np.eye(4)) == compute_rotation_matrix_xyz(np.zeros(3))

