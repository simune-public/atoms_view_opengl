# -*- coding: utf-8 -*-
import os
import sys
sys.path.append('.')
sys.path.append('../src')

try:
    if os.environ["XDG_SESSION_TYPE"] == "wayland":
        os.environ["QT_QPA_PLATFORM"] = "wayland"
except Exception as e:
    """  """

from qtpy.QtCore import QCoreApplication, Qt
from qtpy.QtWidgets import QApplication, QMessageBox, QMainWindow, QFileDialog

from form import Ui_MainWindow as Ui_form
from ase.build import nanotube
from ase.build import molecule
from ase.build import bulk
from ase.constraints import FixAtoms, constrained_indices
from ase.io import read


class mainWindow(QMainWindow):
    def __init__(self, *args):
        super(mainWindow, self).__init__(*args)
        self.ui = Ui_form()
        self.ui.setupUi(self)

    def setup_ui(self):
        # buttons
        self.ui.pushButton1.clicked.connect(self.make_pushButton1)
        self.ui.pushButton2.clicked.connect(self.make_pushButton2)
        self.ui.pushButton3.clicked.connect(self.make_pushButton3)
        self.ui.pushButton4.clicked.connect(self.make_pushButton4)
        self.ui.pushButton5.clicked.connect(self.make_pushButton5)
        self.ui.pushButton6.clicked.connect(self.make_pushButton6)
        self.ui.pushButton7.clicked.connect(self.make_pushButton7)
        self.ui.pushButton8.clicked.connect(self.make_pushButton8)

    def show_error(self, e):
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Critical)
        msg.setText("Error")
        msg.setInformativeText(str(e))
        msg.setWindowTitle("Error")
        msg.exec_()

    def make_pushButton1(self):
        # create model
        model = nanotube(6, 0, length=100)
        self.ui.openGLWidget.set_structure(model)
        self.ui.openGLWidget.update()

    def make_pushButton2(self):
        # destroy model
        self.ui.openGLWidget.clear()
        self.ui.openGLWidget.update()

    def make_pushButton3(self):
        # create model
        model = molecule('CH3CH2NH2')
        model.set_tags([0, -6, 0, 0, 0, 0, -1, 0, 0, 0])
        self.ui.openGLWidget.set_structure(model)
        self.ui.openGLWidget.update()

    def make_pushButton4(self):
        # create model
        model = bulk('Cu', 'fcc', a=3.6, cubic=True)
        self.ui.openGLWidget.plot(model)
        self.ui.openGLWidget.update()

    def make_pushButton5(self):
        # model from file
        fname = QFileDialog.getOpenFileName(self, 'Open file')[0]
        if os.path.exists(fname):
            model = read(fname)
            self.ui.openGLWidget.plot(model)
            self.ui.openGLWidget.update()

    def make_pushButton6(self):
        # rotation
        self.ui.openGLWidget.rotation = "45.0x,0.0y,0.0z"

    def make_pushButton7(self):
        # rotation
        self.ui.openGLWidget.rotation = "0.0x,0.0y,90.0z"

    def make_pushButton8(self):
        # create model with constraint
        model = molecule('CH3CH2NH2')
        c = FixAtoms(indices=[atom.index for atom in model if (atom.symbol == 'N') or (atom.symbol == 'H')])
        model.set_constraint(c)
        self.ui.openGLWidget.plot(model)
        self.ui.openGLWidget.update()


ORGANIZATION_NAME = 'SUSU'
ORGANIZATION_DOMAIN = 'susu.ru'
APPLICATION_NAME = 'example'

QCoreApplication.setApplicationName(ORGANIZATION_NAME)
QCoreApplication.setOrganizationDomain(ORGANIZATION_DOMAIN)
QCoreApplication.setApplicationName(APPLICATION_NAME)

QCoreApplication.setAttribute(Qt.AA_ShareOpenGLContexts)
app = QApplication(sys.argv)
window = mainWindow()
window.setup_ui()
window.show()

sys.exit(app.exec_())
